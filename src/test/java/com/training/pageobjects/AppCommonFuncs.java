package com.training.pageobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class AppCommonFuncs {

	public static WebDriver driver;
	
	/*public  WebDriver AppCommonFuncs() {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
		 PageFactory.initElements(getDriver(), this);
		return driver;
	}
	
	@FindBy(id = "username")
    WebElement username;
*/

	public void set_up_driver() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\swejadhav\\Softwares\\Drivers\\chromedriver.exe");
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\anilakshmi\\SeleniumJars\\ChromeDriver2.9\\chromedriver.exe");
		driver=new ChromeDriver();
	}
		
	public void access_url(String url){
		driver.get(url);
	}
	
	public void login()
	{
		System.out.println(driver.getTitle());
		WebElement username=driver.findElement(By.id("username"));
		waitForElementToBeDisplayed(username);
		WebElement password=driver.findElement(By.id("password"));
		WebElement login_button=driver.findElement(By.id("submit-button"));
		username.clear();
		username.sendKeys("pnutakki");
		password.clear();
		password.sendKeys("password");
		login_button.click();
		waitForResultsLoadingComplete();
				
	}
	public static WebDriver getDriver() {
		return driver;
	}
	
	public static void setDriver(WebDriver driver) {
		AppCommonFuncs.driver = driver;
	}
	
	public void scrollto(WebElement ScrollDownElement) throws InterruptedException{
		
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		Thread.sleep(1000);
		je.executeScript("arguments[0].scrollIntoView(true);",ScrollDownElement);
		// Extract the text and verify
			
	}
	
	public void movetoElementClick(WebElement moveelement)
	{
		new Actions(getDriver()).moveToElement(moveelement).click().perform();
	}
	
	public void selectDate(WebElement icon) throws InterruptedException
	{
		icon.click();
		Thread.sleep(500);
		List<WebElement> allDates=getDriver().findElements(By.xpath(".//*[@id='coral-3']/div/div[2]/table/tbody/tr/td"));
		for(WebElement ele:allDates)
		{
			
			String date=ele.getText();
		
			if(date.equalsIgnoreCase("15"))
			{
				ele.click();
				break;
			}
		}
		
	}
	
	public void waitForElementToBeDisplayed(WebElement element)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 30);
		    wait.until(ExpectedConditions.visibilityOf(element));		    
		}
		catch(Exception e)
		{
			System.out.println("Element not displayed");
		}
	}

	public void waitForWebElementToBeDisplayed(String string_xpath)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 30);
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(string_xpath)));		    
		}
		catch(Exception e)
		{
			System.out.println("Element not displayed");
		}
	}

	public void waitForResultsLoadingComplete()
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 100);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".icon")));
		}
		catch (Exception e)
		{
		}
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 100);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//span[text()='Loading...']")));
		}
		catch (Exception e)
		{
		}

	}
	
	private interface ElementExpectedCondition<T> extends Function<WebElement, T> {

	}
	
	private static Wait<? extends WebElement> wait(WebElement element) {
		return new FluentWait<WebElement>(element).withTimeout(30, TimeUnit.SECONDS).pollingEvery(1,
				TimeUnit.SECONDS);
	}
	
	public static void waitForDisplayed(final WebElement element) {
		wait(element).until(new ElementExpectedCondition<Boolean>() {
			//@Override
			public Boolean apply(WebElement element) {
				try {
					return element.isDisplayed();
				} catch (StaleElementReferenceException e) {
					return null;
				}
			}

			@Override
			public String toString() {
				return String.format("%s to be visible", element);
			}
		});
	}
	
	public boolean isElementPresent(WebElement ele) {
	    try {
	    	ele.isDisplayed();		    	
	        return true;
	    } catch (org.openqa.selenium.NoSuchElementException e) {
	        return false;
	    }
	    catch (Exception e) {
	        return false;
	    }
	}
	
	public void click_Edit_Icon()
	{
		WebElement Global_Edit_Icon=driver.findElement(By.xpath("//coral-button-label[contains(text(),'Edit')]"));
		waitForElementToBeDisplayed(Global_Edit_Icon);
		Global_Edit_Icon.click();
	}
	
	public void click_on_component_configure_button()
	{
		WebElement Component_Config_Edit_Button=driver.findElement(By.xpath("//button[@title='Configure']"));
		waitForElementToBeDisplayed(Component_Config_Edit_Button);

		try {
			scrollto(Component_Config_Edit_Button);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Component_Config_Edit_Button.click();
		//waitForElementToBeDisplayed(Component_Config_Cancel_Button);
	}
	
	public void click_component_config_done() throws InterruptedException
	{
		Thread.sleep(2000);
		WebElement Comp_Config_Done_Button= driver.findElement(By.xpath("//button[@title='Done']"));
		waitForElementToBeDisplayed(Comp_Config_Done_Button);
		Comp_Config_Done_Button.click();

	}
	
	public void publishPage() throws InterruptedException {
		
		Thread.sleep(2000);
		WebElement Properties_Button = driver.findElement(By.xpath("//*[@id='pageinfo-trigger']"));
		
		try {
			waitForElementToBeDisplayed(Properties_Button);
			Properties_Button.click();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			WebDriverWait wait = new WebDriverWait(driver, 240);
			wait.until(ExpectedConditions.elementToBeSelected(Properties_Button));
			Properties_Button.click();
		}
		
		   Thread.sleep(2000);
		   WebElement Publish_Page = driver.findElement(By.xpath("//button/span[contains(text(),'Publish Page')]"));
			movetoElementClick(Publish_Page);
			waitForResultsLoadingComplete();
			
			
			try
			{
				Thread.sleep(2000);
				WebElement publish_button = driver.findElement(By.xpath("//button[contains(text(),'Publish')]"));
				boolean ispresent=isElementPresent(publish_button);			
				System.out.println(ispresent);
				if(ispresent)
				{
					publish_button.click();
				}
				
				
			}
			catch(Exception e)
			{
				System.out.println("Publish button is not available");
			}
			
			
			waitForResultsLoadingComplete();
			
	}
	
	
	public void viewPageAsPublished() throws InterruptedException {
		
		Thread.sleep(2000);
		WebElement Properties_Button = driver.findElement(By.xpath("//*[@id='pageinfo-trigger']"));
		
		try {
			waitForDisplayed(Properties_Button);
			//WebDriverWait wait = new WebDriverWait(driver, 240);
			//wait.until(ExpectedConditions.elementToBeSelected(Properties_Button));
			Properties_Button.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			WebDriverWait wait = new WebDriverWait(driver, 240);
			wait.until(ExpectedConditions.elementToBeSelected(Properties_Button));
			Properties_Button.click();
		}
		
		Thread.sleep(2000);
		WebElement View_As_Published_Button = driver.findElement(By.xpath("//*[@id='pageinfo-data']/button[@title='View as Published']"));
				
		try{
			movetoElementClick(View_As_Published_Button);
		}
		catch(Exception e1)
		{
			WebDriverWait wait = new WebDriverWait(driver, 240);
			wait.until(ExpectedConditions.elementToBeSelected(Properties_Button));
			Properties_Button.click();
			movetoElementClick(View_As_Published_Button);
			
		}

	}
	
	public void login_popover()
	{
		WebElement SDM_Login_Popup = driver.findElement(By.xpath("//button[@class='secondary-button' and contains(text(),'Login')]"));
		waitForDisplayed(SDM_Login_Popup);
		SDM_Login_Popup.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		waitForResultsLoadingComplete();
		
		WebElement Teacher_RadioButton = driver.findElement(By.xpath("//div[@class='teacher-sec']/form/label[@for='teacher']/input[@id='teacher']"));
		waitForDisplayed(Teacher_RadioButton);
		Teacher_RadioButton.click();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement SDM_UserName = driver.findElement(By.xpath("//input[@ng-model='user.username']"));
		WebElement SDM_Password = driver.findElement(By.xpath("//input[@id='loginPasswordInput']"));
		waitForDisplayed(SDM_UserName);
		waitForDisplayed(SDM_Password);
		SDM_UserName.clear();
		SDM_UserName.sendKeys("vivdesai@deloitte.com");
		SDM_Password.clear();
		SDM_Password.sendKeys("hello123");
		
		WebElement SDM_Login = driver.findElement(By.xpath("//button[@id='loginButton']"));
		SDM_Login.click();
		waitForResultsLoadingComplete();
	}
	
	
	public void teardown()
	{
		driver.quit();
	}

}
