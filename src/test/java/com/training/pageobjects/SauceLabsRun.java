package com.training.pageobjects;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SauceLabsRun {
	public static final String USERNAME = "aem-teacher-automate";
	  public static final String ACCESS_KEY = "6451a8eb-2adf-4470-95b5-94a426a91d44";
	  public static final String URL_ = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	 
	public static WebDriver driver_sauce;
	  public static WebDriver getDriver_sauce() {
		return driver_sauce;
	}
	public static void setDriver_sauce(WebDriver driver_sauce) {
		SauceLabsRun.driver_sauce = driver_sauce;
	}
	 
	  public void open_sauce_remote_driver() throws MalformedURLException
	  {
		  DesiredCapabilities caps = DesiredCapabilities.chrome();
		  caps.setCapability("platform", "Windows 10");
		  caps.setCapability("version", "57.0");
	    //URL u=new URL(null);
	 
		  driver_sauce = new RemoteWebDriver(new URL(URL_), caps);
		  driver_sauce.get("https://gmail.com");
		    System.out.println("title of page is: " + driver_sauce.getTitle());
		 
		    driver_sauce.quit();
	 
	  }
	  
}
