package com.training.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class HomePage {

	//LoginPage login=new LoginPage();
	//WebDriver driver=LoginPage.getDriver();
	//WebDriver driver=SauceLabsRun.getDriver_sauce();

	AppCommonFuncs login=new AppCommonFuncs();
	
	WebDriver driver=AppCommonFuncs.getDriver();

	
	private static WebElement homeicon=null;
	private static WebElement sitesLink=null;
	private static WebElement automationFolder=null;
	private static WebElement createButton=null;
	private static WebElement createPage=null;
	private static WebElement articlePage=null;
	private static WebElement articleName=null;
	private static WebElement articleTitle=null;
	private static WebElement articlePageTitle=null;
	private static WebElement articlepageCreateButton=null;
	private static WebElement openButton=null;

	

	public void click_home_button()
	{
		homeicon=driver.findElement(By.xpath("//coral-shell-homeanchor-label[contains(text(),'Adobe Experience Manager')]"));
		homeicon.click();
		login.waitForResultsLoadingComplete();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void click_sites_link()
	{
		sitesLink=driver.findElement(By.xpath("//*[@id='globalnav-home-content']/coral-masonry-item[3]/div/div"));
		sitesLink.click();
		login.waitForResultsLoadingComplete();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void open_automation_folder() throws InterruptedException {

		automationFolder=driver.findElement(By.xpath("//coral-columnview-item-content[text()='Automation']"));
		login.scrollto(automationFolder);
		automationFolder.click();
		login.waitForResultsLoadingComplete();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void click_create_button() throws InterruptedException {

		createButton=driver.findElement(By.xpath("//coral-button-label[text()='Create']"));
		createPage=driver.findElement(By.xpath("(//coral-list-item-content[@class='coral-BasicList-item-content']/coral-icon)[1]"));
		createButton.click();
		System.out.println("Clicked on Create button");
		login.waitForResultsLoadingComplete();
		createPage.click();
		System.out.println("Clicked on create Page");
		login.waitForResultsLoadingComplete();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void click_article_page_template() throws InterruptedException {

		articlePage=driver.findElement(By.xpath("//coral-card-title[contains(text(),'Article page template')]"));
		articlePage.click();
		System.out.println("Clicked on Article page template");
		login.waitForResultsLoadingComplete();
		
		WebElement nextButton=driver.findElement(By.xpath("//coral-button-label[contains(text(),'Next')]"));
		nextButton.click();
		login.waitForResultsLoadingComplete();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void article_page_details() throws InterruptedException {


		articleName=driver.findElement(By.xpath("//input[@name='pageName']"));
		articleTitle=driver.findElement(By.xpath("//input[@name='./jcr:title']"));	
		articlePageTitle=driver.findElement(By.xpath("//input[@name='./pagetitle']"));
		articlepageCreateButton=driver.findElement(By.xpath("//coral-button-label[text()='Create']"));
		
		articleName.clear();
		articleName.sendKeys("AEM_Training_Article");
		
		articleTitle.clear();
		articleTitle.sendKeys("AEM_Training_Article_Title");
		
		articlePageTitle.clear();
		articlePageTitle.sendKeys("AEM Training Article Page Title");
		
		articlepageCreateButton.click();
		login.waitForResultsLoadingComplete();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void open_article_page() throws InterruptedException {

		//WebElement successMessage=driver.findElement(By.xpath("//coral-dialog-header[contains(text(),'Success')]"));
		//WebElement doneButton=driver.findElement(By.xpath("//coral-button-label[contains(text(),'Done')]"));
		openButton=driver.findElement(By.xpath("//coral-button-label[contains(text(),'Open')]"));
		openButton.click();
		login.waitForResultsLoadingComplete();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void create_article_page() throws InterruptedException
	{
		click_home_button();
		click_sites_link();
		open_automation_folder();
		click_create_button();
		Thread.sleep(500);
		AppCommonFuncs.getDriver().navigate().refresh();
		Thread.sleep(500);
		click_article_page_template();
		Thread.sleep(500);
		article_page_details();
		Thread.sleep(500);
		//open_article_page();
	}
	
	public void open_home_to_automation_folder() throws InterruptedException
	{
		click_home_button();
		click_sites_link();
		open_automation_folder();
		
	}
		

}
