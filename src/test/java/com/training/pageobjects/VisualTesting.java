package com.training.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.applitools.eyes.Eyes;
import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;

public class VisualTesting {
	AppCommonFuncs common=new AppCommonFuncs();	
	WebDriver driver=AppCommonFuncs.getDriver();
	ArticlePage article=new ArticlePage();
	Eyes eyes;
	public void open_eyes() throws InterruptedException
	{
		eyes=new Eyes();
		eyes.setApiKey("XzRspcDGibkidvi681VR1027AIzc6nLoQj9vKaq8NJqeQ110");
		eyes.setMatchTimeout(2);
		try{

	           // Start the test and set the browser's viewport size to 800x600.
	           eyes.open(driver, "Hello World!", "VisualTesting Page WebElement",
	                   new RectangleSize(1249, 575));
	           eyes.setForceFullPageScreenshot(true);
	           //eyes.getForceFullPageScreenshot();
	           eyes.setMatchLevel(MatchLevel.STRICT);

	       } finally {

	           // Close the browser.
	          
	       }

	   }
	
	public void verify_fb_page()
	{
		eyes.checkWindow("Facebook");
		
	}
	public void verify_page() throws InterruptedException
	{
		 try
		 {
			 article.open_configured_existing_article_page();
			 eyes.checkWindow("Opened the Page");
		 }
          catch(Exception e)
		 {
        	  System.out.println(e.getMessage());
        	  close_eyes();
		 }

	}
	
	public void verify_portion_page() throws InterruptedException
	{
		
			 
			 article.open_configured_existing_article_page();
			 Thread.sleep(2000);
			 System.out.println("Entered validating element method"); 
			WebElement element=null;
			try
			 {
				 element=driver.findElement(By.xpath("//button[@class='issue-mag-view']"));
			 }
			 catch(Exception e)
			 {
				
				 common.waitForElementToBeDisplayed(element);
			 }
			 
			 
			 System.out.println(element.isDisplayed());
			eyes.checkRegion(element);
			 
		 

	}

	public void close_eyes() {
		driver.quit();
		try {
			eyes.close();
		} catch (Exception e) {
			e.printStackTrace();
			// If the test was aborted before eyes.close was called, ends the
			// test as aborted.
			eyes.abortIfNotClosed();
		}
	}

	

}
