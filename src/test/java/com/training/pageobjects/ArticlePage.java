package com.training.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



import junit.framework.Assert;

public class ArticlePage {

	
	AppCommonFuncs common=new AppCommonFuncs();	
	WebDriver driver=AppCommonFuncs.getDriver();

	//LoginPage login=new LoginPage();	
	//WebDriver driver=common.getDriver();
	public void open_configured_existing_article_page() throws InterruptedException
	{
	
		String url="https://sn4-aem-qa.scholastic.com/content/automation/sn4/issues/2016-17/032717.html?";
		driver.get(url);
		//LoginPage home=new LoginPage();
		common.waitForResultsLoadingComplete();
	}

	public void open_existing_article_page() throws InterruptedException
	{
	
		WebElement existing_article_page=driver.findElement(By.xpath("//coral-columnview-item-content[contains(text(),'AEM_Training_Article_Title')]/preceding-sibling::coral-columnview-item-thumbnail"));
		common.scrollto(existing_article_page);
		common.waitForElementToBeDisplayed(existing_article_page);
		existing_article_page.click();
		common.click_Edit_Icon();
		common.waitForResultsLoadingComplete();
	}
	
	public void open_existing_article_page_in_action() throws InterruptedException
	{
	
		WebElement Action_Folder = driver.findElement(By.xpath("//coral-columnview-item-content[contains(text(),'Action')]"));
		common.scrollto(Action_Folder);
		common.waitForElementToBeDisplayed(Action_Folder);
		Action_Folder.click();	
		
		Thread.sleep(2000);
		WebElement Issues_Folder = driver.findElement(By.xpath("//coral-columnview-item-content[contains(text(),'Issues')]"));
		common.scrollto(Issues_Folder);
		common.waitForElementToBeDisplayed(Issues_Folder);
		Issues_Folder.click();
		
		Thread.sleep(2000);
		WebElement Issues_2017 = driver.findElement(By.xpath("//coral-columnview-item-content[contains(text(),'2016-17')]"));
		common.scrollto(Issues_2017);
		common.waitForElementToBeDisplayed(Issues_2017);
		Issues_2017.click();
		
		Thread.sleep(2000);
		WebElement March27_Folder = driver.findElement(By.xpath("//coral-columnview-item-content[contains(text(),'March 27')]"));
		common.scrollto(March27_Folder);
		common.waitForElementToBeDisplayed(March27_Folder);
		March27_Folder.click();
		
		Thread.sleep(2000);
		WebElement existing_article_page=driver.findElement(By.xpath("//coral-columnview-item-content[contains(text(),'AEM_Training_Article_Title_')]/preceding-sibling::coral-columnview-item-thumbnail"));
		common.scrollto(existing_article_page);
		common.waitForElementToBeDisplayed(existing_article_page);
		existing_article_page.click();
		
		Thread.sleep(2000);
		common.click_Edit_Icon();
		common.waitForResultsLoadingComplete();
		
	}
	
	public void drag_and_drop_article_text_component() throws InterruptedException
	{
		WebElement SourceLocator=driver.findElement(By.xpath("//h4[text()='Article - Image']"));
		//WebElement DestinationLocator=driver.findElement(By.xpath(".//*[@id='OverlayWrapper']/div[3]/div"));
		WebElement DestinationLocator=driver.findElement(By.xpath("(//div[@data-text='Drag components here'])[5]"));
		String component_name="article";
		drag_and_drop_component(SourceLocator,DestinationLocator,component_name);
		
	}
	
	public void drag_and_drop_article_sidebar_component() throws InterruptedException
	{
		WebElement SourceLocator=driver.findElement(By.xpath("//h4[text()='Article - Sidebar']"));
		WebElement DestinationLocator=driver.findElement(By.xpath("(//div[@data-text='Drag components here'])[5]"));
		
		//WebElement DestinationLocator=driver.findElement(By.xpath(".//*[@id='OverlayWrapper']/div[3]/div"));
		String component_name="sidebar";
		drag_and_drop_component(SourceLocator,DestinationLocator,component_name);
		
	}
	
	public void drag_and_drop_component(WebElement Sourcelocator,WebElement Destinationlocator,String componentname) throws InterruptedException
	{
		WebElement component_filter=driver.findElement(By.xpath("//input[@placeholder='Filter']"));
		component_filter.sendKeys(componentname);
		component_filter.sendKeys(Keys.ENTER);
		AppCommonFuncs home=new AppCommonFuncs();
		home.waitForElementToBeDisplayed(Sourcelocator);
		home.scrollto(Sourcelocator);
		Actions builder = new Actions(driver);
		org.openqa.selenium.interactions.Action dragAndDrop = builder.clickAndHold(Sourcelocator)
					.moveToElement(Destinationlocator).release(Destinationlocator).build();
			dragAndDrop.perform();
		AppCommonFuncs.getDriver().navigate().refresh();
		home.waitForResultsLoadingComplete();
			
	}
	
	public void switchwindow(int x) throws InterruptedException {
		String windowHandle = driver.getWindowHandle();

		// Get the list of window handles to create a page using components in
		// new tab
		ArrayList tabs = new ArrayList(driver.getWindowHandles());

		// Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabs.get(x));

		if (x == 1) {
			WebDriverWait wait = new WebDriverWait(driver, 240);
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(".//*[@id='Content']/div[1]/nav/div[1]/button[1]")));
			//WaitUtils.waitForEnabled(TogglePanel_Button);
			WebElement TogglePanel_Button=driver.findElement(By.xpath(".//*[@id='Content']/div[1]/nav/div[1]/button[1]"));
			AppCommonFuncs login=new AppCommonFuncs();
			login.waitForElementToBeDisplayed(TogglePanel_Button);
			TogglePanel_Button.click();
			Thread.sleep(2000);

		}

	}
	public void waitforArticlePageLoded()
	{ 
		try {
			WebElement Global_Components_Button=driver.findElement(By.xpath(".//*[@id='SidePanel']/div[2]/div[1]/nav/a[2]"));
			Global_Components_Button.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			common.waitForResultsLoadingComplete();
			WebElement TogglePanel_Button=driver.findElement(By.xpath(".//*[@id='Content']/div[1]/nav/div[1]/button[1]"));
			common.waitForElementToBeDisplayed(TogglePanel_Button);
			TogglePanel_Button.click();
			WebElement Global_Components_Button=driver.findElement(By.xpath(".//*[@id='SidePanel']/div[2]/div[1]/nav/a[2]"));
			common.waitForElementToBeDisplayed(Global_Components_Button);
			Global_Components_Button.click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Clicked on Components Button");

		}
	}

	public void click_create_article_sidebar_component()
	{
		
		try {
			WebElement Article_SideBar_Component=driver.findElement(By.xpath("//div[contains(@data-path,'/article_sidebar')]"));
			Article_SideBar_Component.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	
	}
	
	public void search_for_image_asset(String imagename,WebElement image) throws InterruptedException
	{
		WebElement Global_Assets_Button=driver.findElement(By.xpath("//div[@id='SidePanel']/div[2]/div/nav/a[@title='Assets']/i"));
		common.waitForElementToBeDisplayed(Global_Assets_Button);
		Global_Assets_Button.click();
		
		WebElement Asset_KeywordSearch=driver.findElement(By.xpath("//*[@id='assetsearch']"));
		common.waitForElementToBeDisplayed(Asset_KeywordSearch);
		Asset_KeywordSearch.sendKeys(imagename);
		Asset_KeywordSearch.sendKeys(Keys.ENTER);
		common.scrollto(image);
		
	}
	
	public void article_sidebar_configure_title() throws InterruptedException
	{
		Thread.sleep(2000);
		WebElement Sidebar_Component_Overlay = driver.findElement(By.xpath("//form[contains(@action,'/article_sidebar')]"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Overlay);
		
		//Tabs
		WebElement Sidebar_Component_Text_Tab = driver.findElement(By.xpath("//a[contains(text(),'Text')]"));
		
		//Navigate to the Text tab and back to Title Tab
		common.waitForElementToBeDisplayed(Sidebar_Component_Text_Tab);
		Sidebar_Component_Text_Tab.click();
		WebElement Sidebar_Component_Text_Tab_SectionTitle = driver.findElement(By.xpath("//input[@name='./sectionTitle']"));
		Sidebar_Component_Text_Tab_SectionTitle.click();
		
		WebElement Sidebar_Component_Title_Tab = driver.findElement(By.xpath("//a[contains(text(),'Title')]"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Title_Tab);
		Sidebar_Component_Title_Tab.click();
	
		//Enter the Title details
		WebElement Sidebar_Component_Title_Tab_Title = driver.findElement(By.xpath("//input[@name='./title']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Title_Tab_Title);
		Sidebar_Component_Title_Tab_Title.clear();
		Sidebar_Component_Title_Tab_Title.sendKeys("Article Title");
		
		WebElement Sidebar_Component_Title_Tab_FontColor = driver.findElement(By.xpath("//input[@name='./titleFontColor']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Title_Tab_FontColor);
		Sidebar_Component_Title_Tab_FontColor.clear();
		Sidebar_Component_Title_Tab_FontColor.sendKeys("#000000");
		
		WebElement Sidebar_Component_Title_Tab_BackgroundColor = driver.findElement(By.xpath("//input[@name='./titleBackgroundColor']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Title_Tab_BackgroundColor);
		Sidebar_Component_Title_Tab_BackgroundColor.clear();
		Sidebar_Component_Title_Tab_BackgroundColor.sendKeys("#8f007a");
		
		WebElement Sidebar_Component_Title_Tab_FontSize = driver.findElement(By.xpath("//input[@name='./titleFontSize']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Title_Tab_FontSize);
		Sidebar_Component_Title_Tab_FontSize.clear();
		Sidebar_Component_Title_Tab_FontSize.sendKeys("20px");
		
		WebElement Sidebar_Component_Title_Tab_Alignment = driver.findElement(By.xpath("//select[@name='./titleAlignment']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Title_Tab_Alignment);
		Sidebar_Component_Title_Tab_Alignment.sendKeys("Left");
				
	}
	
	public void article_sidebar_configure_text() throws InterruptedException
	{
		//Tabs
		WebElement Sidebar_Component_Text_Tab = driver.findElement(By.xpath("//a[contains(text(),'Text')]"));
		
		//Navigate to the Text tab
		common.waitForElementToBeDisplayed(Sidebar_Component_Text_Tab);
		Sidebar_Component_Text_Tab.click();
	
		//Enter the Text details
		
		WebElement Sidebar_Component_Text_Tab_FontColor = driver.findElement(By.xpath("//input[@name='./fontColor']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Text_Tab_FontColor);
		Sidebar_Component_Text_Tab_FontColor.clear();
		Sidebar_Component_Text_Tab_FontColor.sendKeys("#000000");
		
		WebElement Sidebar_Component_Text_Tab_FontSize = driver.findElement(By.xpath("//input[@name='./fontSize']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Text_Tab_FontSize);
		Sidebar_Component_Text_Tab_FontSize.clear();
		Sidebar_Component_Text_Tab_FontSize.sendKeys("10px");
		
		WebElement Sidebar_Component_Text_Tab_Alignment = driver.findElement(By.xpath("//select[@name='./textAlignment']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Text_Tab_Alignment);
		Sidebar_Component_Text_Tab_Alignment.sendKeys("Top");
		
		WebElement Sidebar_Component_Text_Tab_Background_Color = driver.findElement(By.xpath("//input[@name='./backgroundColor']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Text_Tab_Background_Color);
		Sidebar_Component_Text_Tab_Background_Color.clear();
		Sidebar_Component_Text_Tab_Background_Color.sendKeys("#f8ede6");
				
	}
	
	public void article_sidebar_configure_image() throws InterruptedException
	{
		//Tabs
		WebElement Sidebar_Component_Image_Tab = driver.findElement(By.xpath("//a[@class='coral-TabPanel-tab' and contains(text(),'Image')]"));
		
		//Navigate to the Image tab
		common.waitForElementToBeDisplayed(Sidebar_Component_Image_Tab);
		Sidebar_Component_Image_Tab.click();
	
		//Enter the Image details details
		
		WebElement Sidebar_Component_Image_Tab_Credits = driver.findElement(By.xpath("//input[@name='./credits']"));
		common.waitForElementToBeDisplayed(Sidebar_Component_Image_Tab_Credits);
		Sidebar_Component_Image_Tab_Credits.clear();
		Sidebar_Component_Image_Tab_Credits.sendKeys("Getty Images");
		
	}
	

	public void draganddropArticleImage()
	{
		WebElement assetsTab = driver.findElement(By.xpath("//a[@title='Assets']"));
		common.waitForElementToBeDisplayed(assetsTab);
		assetsTab.click();
		
		WebElement source = driver.findElement(By.xpath("(//a[@class='label'])[1]"));
		WebDriverWait wait = new WebDriverWait(driver, 240);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//a[@class='label'])[1]")));
		
        WebElement target = driver.findElement(By.xpath("(//span[@data-init='fileupload'])[1]/div[@class='cq-FileUpload-thumbnail']"));
        Actions builder = new Actions(driver);
		org.openqa.selenium.interactions.Action dragAndDrop = builder.clickAndHold(source)
				.moveToElement(target).release(target).build();
		dragAndDrop.perform();  

	}
	
	
	public void verify_article_sidebar_css() throws InterruptedException
	{
		common.waitForResultsLoadingComplete();
		Thread.sleep(3000);	
		
		//WebElement Article_Sidebar_Title = driver.findElement(By.xpath("//div[@class='article-section-title']"));
		
		WebElement Article_Sidebar_Title = driver.findElement(By.xpath("//div[text()='Article Title']"));
		common.waitForElementToBeDisplayed(Article_Sidebar_Title);
		System.out.println("Article Sidebar Title Font Family: " + Article_Sidebar_Title.getCssValue("font-family"));
		Assert.assertEquals(Article_Sidebar_Title.getCssValue("font-family"), "GeometricSlab703BT-XtraBold");
		
		System.out.println("Article Sidebar Title Font Size: " + Article_Sidebar_Title.getCssValue("font-size"));
		Assert.assertEquals(Article_Sidebar_Title.getCssValue("font-size"), "20px");
		
		System.out.println("Article Sidebar Title Font Color: " + Article_Sidebar_Title.getCssValue("color"));
		Assert.assertEquals(Article_Sidebar_Title.getCssValue("color"), "rgba(0, 0, 0, 1)");
		
		System.out.println("Article Sidebar Title Background Color: " + Article_Sidebar_Title.getCssValue("background-color"));
		Assert.assertEquals(Article_Sidebar_Title.getCssValue("background-color"), "rgba(143, 0, 122, 1)");
		
		/*
		WebElement Article_Sidebar_Text = driver.findElement(By.xpath("//div[@class='article-section-text']/p"));
		
		common.waitForElementToBeDisplayed(Article_Sidebar_Text);
		System.out.println("Article Sidebar Text Font Family: " + Article_Sidebar_Text.getCssValue("font-family"));
		Assert.assertEquals(Article_Sidebar_Text.getCssValue("font-family"), "Geometric706BT-MediumB");
		
		System.out.println("Article Sidebar Text Font Size: " + Article_Sidebar_Text.getCssValue("font-size"));
		Assert.assertEquals(Article_Sidebar_Text.getCssValue("font-size"), "10px");
		
		System.out.println("Article Sidebar Text Font Color: " + Article_Sidebar_Text.getCssValue("color"));
		Assert.assertEquals(Article_Sidebar_Text.getCssValue("color"), "rgba(0, 0, 0, 1)");
		*/
		
		WebElement Article_Sidebar_Image = 	driver.findElement(By.xpath("//div[@class='image-icon-container']/sw-image/div/img"));
		common.waitForElementToBeDisplayed(Article_Sidebar_Image);
		System.out.println("Article Sidebar Image Source: " + Article_Sidebar_Image.getAttribute("src"));
		Assert.assertTrue(Article_Sidebar_Image.getAttribute("src").contains("/content/dam/classroom-magazines/wethepeople/homepage/desktop-images/upfront-logo.png"));
		
		
		WebElement Article_Sidebar_Credits = driver.findElement(By.xpath("//div[@class='image-credit']"));
		common.waitForElementToBeDisplayed(Article_Sidebar_Credits);
		System.out.println("Article Sidebar Credits Font Family: " + Article_Sidebar_Credits.getCssValue("font-family"));
		Assert.assertEquals(Article_Sidebar_Credits.getCssValue("font-family"), "Geometric706BT-MediumB");
		
		System.out.println("Article Sidebar Credits Font Size: " + Article_Sidebar_Credits.getCssValue("font-size"));
		Assert.assertEquals(Article_Sidebar_Credits.getCssValue("font-size"), "10px");
		
		System.out.println("Article Sidebar Credits Font Color: " + Article_Sidebar_Credits.getCssValue("color"));
		Assert.assertEquals(Article_Sidebar_Credits.getCssValue("color"), "rgba(70, 70, 70, 1)");
		
	}
	
	
	public void click_drag_components_to_configure()
	{	
		try {
			WebElement Drag_Components=driver.findElement(By.xpath("(//div[@title='Drag components here'])[2]"));
			common.waitForElementToBeDisplayed(Drag_Components);
			Drag_Components.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
	
	public void click_add_components()
	{
		try {
			WebElement Add_Component = driver.findElement(By.xpath("//button[@data-action='INSERT']"));
			common.waitForElementToBeDisplayed(Add_Component);
			Add_Component.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insert_new_components(String componentName)
	{
		WebElement Insert_Component_Overlay = driver.findElement(By.xpath("//div[@class='coral-Modal-body InsertComponentDialog-components']"));
		common.waitForElementToBeDisplayed(Insert_Component_Overlay);
		
		WebElement components_List=driver.findElement(By.xpath("//ul[@class='coral-SelectList-sublist']"));
		List<WebElement> all_component_names = components_List.findElements(By.tagName("button"));
		
		System.out.println(componentName);
		
		for (int i=0;i<all_component_names.size();i++)
		{
			System.out.println(all_component_names.get(i).getText());
			if(all_component_names.get(i).getText().equalsIgnoreCase(componentName))
			{
				all_component_names.get(i).click();
				break;
			}
			
			else
			{
				System.out.println("No such component found to add");
			}
		}
		
	}
	
	
	public void verify_component_created()
	{
		WebElement Article_SideBar_Component=driver.findElement(By.xpath("//div[contains(@data-path,'/article_sidebar')]"));
		Assert.assertTrue(Article_SideBar_Component.isDisplayed());
	}

}
