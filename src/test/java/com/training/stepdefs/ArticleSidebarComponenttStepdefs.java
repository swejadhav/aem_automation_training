package com.training.stepdefs;


import com.training.pageobjects.ArticlePage;
import com.training.pageobjects.HomePage;

import org.openqa.selenium.WebDriver;

import com.training.pageobjects.AppCommonFuncs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ArticleSidebarComponenttStepdefs {


	AppCommonFuncs common=new AppCommonFuncs();
	ArticlePage articlepage=new ArticlePage();
	
	@And("^drags and drops a new Article Sidebar Component on the page\\.$")
	public void user_clicks_on_to_configure_a_new_Article_Sidebar_Component_on_the_page() throws Throwable {
		
		articlepage.drag_and_drop_article_sidebar_component();
		common.teardown();
	}
	
	@When("^user clicks on to configure a new Article Sidebar Component on the page\\.$")
	public void user_clicks_on_to_configure_a_new_Article_text_Image_Component_on_the_page() throws Throwable {

		articlepage.click_create_article_sidebar_component();
		common.click_on_component_configure_button();

	}

	@Then("^the user should be able to configure the title, text and the image in the component\\.$")
	public void the_user_should_be_able_to_configure_the_title_in_the_component() throws Throwable {

		articlepage.article_sidebar_configure_title();
		articlepage.article_sidebar_configure_text();
		articlepage.article_sidebar_configure_image();
		articlepage.draganddropArticleImage();	
		common.click_component_config_done();
	}
	
	@And("^user publishes the page\\.$")
	public void user_publishes_the_page() throws Throwable {
		
		common.publishPage();
		articlepage.switchwindow(1);
		Thread.sleep(3000);
		//articlepage.waitforArticlePageLoded();
		
	}

	@And("^the user views the page in published mode\\.$")
	public void the_user_views_the_page_in_published_mode() throws Throwable {

		common.viewPageAsPublished();
		common.waitForResultsLoadingComplete();
		articlepage.switchwindow(2);
		common.waitForResultsLoadingComplete();

	}
	
	@And("^user logs in to the published page\\.$")
	public void the_user_logs_in_to_the_published_page() throws Throwable {

		common.login_popover();
	}

	@Then("^the user should be able to view the article sidebar component with the configured sepcifications\\.$")
	public void the_user_should_be_able_to_view_the_article_sidebar_component_with_the_configured_sepcifications() throws Throwable {
		
		articlepage.verify_article_sidebar_css();
		common.teardown();
	}


}
