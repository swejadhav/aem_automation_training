package com.training.stepdefs;

import org.openqa.selenium.WebDriver;

import com.training.pageobjects.AppCommonFuncs;
import com.training.pageobjects.VisualTesting;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class VisualTestingFBStepdefs {
	
	AppCommonFuncs login=new AppCommonFuncs();
	VisualTesting visual=new VisualTesting();
	
	
	@Then("^user should ensure that the design is correct\\.$")
	public void user_should_ensure_that_the_design_is_correct() throws Throwable {
		
		visual.open_eyes();
		visual.verify_fb_page();
		visual.close_eyes();
	}


}
