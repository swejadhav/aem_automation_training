package com.training.stepdefs;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;

import com.training.pageobjects.HomePage;
import com.training.pageobjects.SauceLabsRun;
import com.training.pageobjects.AppCommonFuncs;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DemoStepdefs {

	
	SauceLabsRun sauce=new SauceLabsRun();
	AppCommonFuncs common=new AppCommonFuncs();


	
	@Given("^user logs into the aem application as author.$")
	public void login_aem_author()
	{
		System.out.println("Entered Given");
		String url="http://authoraem-qa.scholastic.com";
		common.set_up_driver();
		common.access_url(url);
		common.login();
		AppCommonFuncs.getDriver().manage().window().maximize();
		
	}
	@Given("^user logs into facebook\\.$")
	public void user_logs_into_facebook() throws Throwable {
		
		String url="http://authoraem-qa.scholastic.com";
		login.set_up_driver();
	    login.access_url(url);
	    AppCommonFuncs.getDriver().manage().window().maximize();
	    login.waitForResultsLoadingComplete();
	}
	
	@When("^author creates a new article page with article component.$")
	public void create_new_article_page() throws InterruptedException
	{
		System.out.println("Entered When");
		HomePage home=new HomePage();
		home.create_article_page();
		
	}

	@Then("^page should be created with the required components$")
	public void created_page_ang_drag_and_drop_component() throws InterruptedException
	{
		System.out.println("Entered Then");
		HomePage home=new HomePage();
		home.open_article_page();
		common.teardown();
	}
}
