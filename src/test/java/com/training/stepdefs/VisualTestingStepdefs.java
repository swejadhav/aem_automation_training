package com.training.stepdefs;

import com.training.pageobjects.ArticlePage;
import com.training.pageobjects.VisualTesting;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VisualTestingStepdefs {
	VisualTesting visual=new VisualTesting();
	
	
	@When("^author opens existing article page\\.$")
	public void author_opens_existing_article_page() throws Throwable {
		ArticlePage articlepage=new ArticlePage();
		articlepage.open_configured_existing_article_page();
		visual.open_eyes();
	}

	@When("^sets the visual test with \"([^\"]*)\" mode\\.$")
	public void sets_the_visual_test_with_mode(String arg1) throws Throwable {
	    
	    
	}

	@Then("^the editor should ensure that the gamer icon in the sn(\\d+) is as per the spritesheet\\.$")
	public void the_editor_should_ensure_that_the_gamer_icon_in_the_sn_is_as_per_the_spritesheet(int arg1) throws Throwable {
	    
		visual.verify_page();
		
		
	}
	@Then("^the editor should ensure that the portion of page is as per the design\\.$")
	public void portion_of_page_as_per_design() throws Throwable {
	    
		visual.verify_portion_page();
		//visual.verify_page();
		
	}

	@Then("^tears down the eye$")
	public void tears_down_the_eyes() throws Throwable {
	    
	    visual.close_eyes();
	}
	
	
	

}
