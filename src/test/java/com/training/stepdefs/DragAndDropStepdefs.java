package com.training.stepdefs;

import com.training.pageobjects.ArticlePage;
import com.training.pageobjects.HomePage;
import com.training.pageobjects.AppCommonFuncs;


import cucumber.api.java.en.When;

public class DragAndDropStepdefs {

	@When("^article opens automation folder\\.$")
	public void author_opens_automation_folder() throws Throwable {
	    HomePage home=new HomePage();
	    home.open_home_to_automation_folder();
	    
	}
	@When("^author opens article page\\.$")
	public void author_opens_article_page() throws Throwable {
	    
	    ArticlePage articlepage=new ArticlePage();
	    articlepage.open_existing_article_page_in_action();
	    articlepage.switchwindow(1);
	    articlepage.waitforArticlePageLoded();
	}
	@When("^drags and drops the component\\.$")
	public void drags_and_drops_the_component() throws Throwable {
	    
		AppCommonFuncs common=new AppCommonFuncs();
		ArticlePage articlepage=new ArticlePage();
		articlepage.drag_and_drop_article_text_component();

		common.teardown();
		// articlepage.drag_and_drop_component(Sourcelocator, Destinationlocator, componentname);
	    
	}

}
