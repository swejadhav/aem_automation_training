package com.training.stepdefs;

import com.training.pageobjects.ArticlePage;
import com.training.pageobjects.AppCommonFuncs;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddComponenttStepdefs {
	
	AppCommonFuncs common=new AppCommonFuncs();
	ArticlePage articlepage=new ArticlePage();

	@When("^user clicks on the Drag Components bar\\.$")
	public void user_clicks_on_the_Drag_Components_bar() throws Throwable {
		
		articlepage.click_drag_components_to_configure();

	}

	@When("^selects the Add symbol to add a component\\.$")
	public void selects_the_Add_symbol_to_add_a_component() throws Throwable {
		
		articlepage.click_add_components();

	}

	@When("^selects a component type from the Insert Component menu to add a component.$")
	public void selects_a_component_type_from_the_Insert_Component_menu() throws Throwable {
		
		articlepage.insert_new_components("Article - Sidebar");
		Thread.sleep(3000);
		common.teardown();

	}



}
