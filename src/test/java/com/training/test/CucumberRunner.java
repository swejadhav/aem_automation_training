package com.training.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/automation/cucumber.json",
        overviewReport = true,
        outputFolder = "target")

/*@RunWith(Cucumber.class)*/

@CucumberOptions(features = "Features", dryRun= false ,monochrome=true,

glue={"com.training.stepdefs"},
format = {
		"pretty", "html:target/automation/html",
		"json:target/automation/cucumber.json",
		"junit:target/automation/taget_junit/HomePageTestXml.xml" }) 

public class CucumberRunner {
	

}





