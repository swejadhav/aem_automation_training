Feature: Training Demo

@trainingtest4
Scenario:  To check if the author can create and configure a article sidebar component in the article page.
Given user logs into the aem application as author.
And article opens automation folder.
And author opens article page.
When user clicks on to configure a new Article Sidebar Component on the page.
And the user should be able to configure the title, text and the image in the component.
And user publishes the page.
And the user views the page in published mode.
And user logs in to the published page.
Then the user should be able to view the article sidebar component with the configured sepcifications.

