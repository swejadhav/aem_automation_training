Feature: Training Demo

@trainingtest2
Scenario:  To check if the author can drag an drop a component in the article page.
Given user logs into the aem application as author.
When article opens automation folder.
When author opens article page.
And drags and drops the component.

@trainingtest3
Scenario:  To check if the author can drag and drop a article sidebar component in the article page.
Given user logs into the aem application as author.
And article opens automation folder.
And author opens article page.
And drags and drops a new Article Sidebar Component on the page.