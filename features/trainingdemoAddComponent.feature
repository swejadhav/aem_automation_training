Feature: Training Demo

@trainingtest5
Scenario:  To check if the author can add a new component using the Drag Components Bar
Given user logs into the aem application as author.
And article opens automation folder.
And author opens article page.
When user clicks on the Drag Components bar.
And selects the Add symbol to add a component.
Then selects a component type from the Insert Component menu to add a component.
 

