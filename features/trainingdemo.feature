Feature: Training Demo

@trainingtest1
Scenario:  To check if the author can design the page using article components
Given user logs into the aem application as author.
When author creates a new article page with article component.
Then page should be created with the required components.
